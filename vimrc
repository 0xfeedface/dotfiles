" ------------------------------------------------------------------------------
" Plugin-specific settings
" ------------------------------------------------------------------------------

" Single space after comment character
let g:NERDSpaceDelims=1
let g:NERDCustomDelimiters={ 'robot': { 'left': '#' } }

let t_Co=16
let g:solarized_contrast='high'
let g:solarized_visibility='low'

let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_confirm_extra_conf=0

let g:airline#extensions#whitespace#mixed_indent_algo = 1

let g:vim_json_syntax_conceal = 0

let g:go_highlight_operators = 1
let g:go_highlight_extra_types = 1

let g:CommandTFileScanner = "git"
let g:CommandTMatchWindowAtTop = 1
let g:CommandTSmartCase = 1

" let g:unite_source_history_yank_enable = 1
" try
    " let g:unite_source_rec_async_command = 'ag --nocolor --nogroup -g ""'
    " call unite#filters#matcher_default#use(['matcher_fuzzy'])
" catch
" endtry
" " search a file in the filetree
" nnoremap <space><space> :split<cr> :<C-u>Unite -start-insert
" file_rec/async<cr>
" " reset not it is <C-l> normally
" :nnoremap <space>r <Plug>(unite_restart)

" ------------------------------------------------------------------------------
" Vundle setup
" ------------------------------------------------------------------------------

" disable VI compatibility mode
set nocompatible
" Disable file type plugins during Vundle setup
filetype off

set rtp+=~/.vim/bundle/Vundle
call vundle#begin()

Plugin '0xfeedface/robotframework-vim'
Plugin '0xfeedface/vim-colors-solarized'
Plugin 'Valloric/YouCompleteMe'
Plugin 'bkad/CamelCaseMotion'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'derekwyatt/vim-fswitch'
Plugin 'elzr/vim-json'
Plugin 'fatih/vim-go'
Plugin 'gmarik/Vundle'
Plugin 'godlygeek/tabular'
Plugin 'scrooloose/nerdcommenter'
Plugin 'tpope/vim-eunuch'
Plugin 'tpope/vim-surround'
Plugin 'wincent/command-t'
" Plugin 'Shougo/vimproc.vim'
" Plugin 'Shougo/unite.vim'
Plugin 'wting/rust.vim'
Plugin 'maksimr/vim-jsbeautify'
Plugin 'einars/js-beautify'
Plugin 'dhruvasagar/vim-table-mode'

call vundle#end()

call camelcasemotion#CreateMotionMappings(',')

" ------------------------------------------------------------------------------
" Vim settings
" ------------------------------------------------------------------------------

" Enable filetype plugin
filetype plugin indent on

" Enable syntax highlighting
syntax on

" set leader key
let mapleader=' '

noremap<C-h> :tabprevious<CR>
noremap<C-l> :tabnext<CR>

" Set default tabbing options
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

" <C-T> and <C-A> round to next shiftwidth
set shiftround

" Enable incremental search highlight
set incsearch

" Enable wildcard menu for command-line
set wildmenu

" Enable modelines
set modeline

" Number formats for <C-A> and <C-X>
set nrformats=alpha,hex

" Enable mouse support in TTY
set mouse=a

" Minimum distance of current line from window top or bottom
set scrolloff=2

" Automatically reload files changed outside of Vim
set autoread

" Disable folding
set nofoldenable

" Allow backspacing over everything in insert mode
set backspace=indent,eol,start

" Keep backups
set backup

" Store backups in one place
set backupdir=~/.vim/backup

" Store swap files in one place
set dir=~/.vim/swap

" Highlight current line
set cursorline

" Highlight column 80
set colorcolumn=80

" Enable line, column number in corner
set ruler

" Set UTF-8 encoding
set enc=utf-8

" Use indentation of previous line
set autoindent
" set cindent

" Case-insensitive searching
set ignorecase
" Case-sensitive search if Capital letters are searched
set smartcase

" enable line numbers
set relativenumber
set number
set numberwidth=5

" Complete options (disable preview scratch window)
set completeopt=menu,menuone,longest

" Highlight matching braces
set showmatch

" Disable bell
set visualbell

" Reducde Vim's verbosity
set shortmess=I

" Show status line
set laststatus=2

" show invisibles
set list
" Use the same symbols as TextMate for tabstops and EOLs
set listchars=nbsp:·,tab:▸\ ,eol:¬

" Limit popup menu height in insert mode completion
set pumheight=15

" Solarize!
set background=dark
colorscheme solarized

" ------------------------------------------------------------------------------
" Leader key mappings
" ------------------------------------------------------------------------------

" Quick editing of .vimrc
map<leader>rc  :tabedit ~/.vimrc<CR>
map<leader>grc :tabedit ~/.gvimrc<CR>

map<leader>r :!rubber-- pdf - fq % <CR><CR>

" Some key mappings
map<leader>se  :setlocal spell spelllang=en_us<CR>
map<leader>sb  :setlocal spell spelllang=en_uk<CR>
map<leader>sd  :setlocal spell spelllang=de<CR>
map<leader>md  :setlocal filetype=markdown<CR>
map<leader>o   :FSHere<CR>
map<leader>h   :set nohlsearch<CR>
map<leader>tc  :Tab / :\s*\zs\S\+<CR>
" map <leader>cl  :sp /System/Library/Frameworks/OpenCL.framework/Headers/cl.h<CR>

" ------------------------------------------------------------------------------
" Auto commands
" ------------------------------------------------------------------------------

if (has("autocmd"))

    " When vimrc is saved, reload it
    au! BufWritePost ~/.vimrc source ~/.vimrc

    " If we run under the GUI, also reload GUI-specific settings
    if (has("gui_running"))
        au! BufWritePost ~/.vimrc source ~/.gvimrc
    endif

    " Filetype-dependent whitespace definitions
    au! FileType xml,html,css,plain,tex,json    setlocal ts=2 sts=2 sw=2 expandtab
    au! FileType vim setlocal keywordprg=:help

    au! BufNewFile,BufRead *.h   let b:fswitchdst="cc,cpp,c" | let b:fswitchlocs=".,../src"
    au! BufNewFile,BufRead *.hh  let b:fswitchdst="cc"       | let b:fswitchlocs=".,../src"
    au! BufNewFile,BufRead *.cc  let b:fswitchdst="h,hh"     | let b:fswitchlocs=".,../include"
    au! BufNewFile,BufRead *.cpp let b:fswitchdst="h"        | let b:fswitchlocs=".,../include"
    au! BufNewFile,BufRead *.c   let b:fswitchdst="h"        | let b:fswitchlocs=".,../include"

    au! FileType c,cpp map<leader>f :pyf /home/norman/.vim/clang-format.py<CR>
                     " imap<leader> f<ESC> :pyf /home/norman/.vim/clang-format.py<CR>i

endif  " autocmd

" ------------------------------------------------------------------------------
" Functions
" ------------------------------------------------------------------------------

" Show syntax highlighting groups for word under cursor
nmap <leader>p :call <SID>SynStack()<CR>
function! <SID>SynStack()
    if !exists("*synstack")
        return
    endif
    echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc

" Remove trailing whitespace
nmap <leader>w :call RemoveTrailingWhitespace()<CR>
function! RemoveTrailingWhitespace()
    :%s/\s\+$//ge
endfunction

